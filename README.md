# MIEI | 2ºano 1º Semestre | Links and Resources

Vários professores usam diferentes links e as vezes pode ficar dificil guardá-los todos, fiz entao uma lista com todos os recursos de cada disciplina.

## AED | Algoritmos e Estruturas de Dados

- **Aulas:**

Plataforma: Zoom  
Link: https://videoconf-colibri.zoom.us/j/95580579579?pwd=WHQ1czYxbHNDWk1SZFhKM21vTFVUUT09

--//--

Meeting ID: 955 8057 9579  
Password: 3875  

Tambem usado para hórario de dúvidas

- **Moodle:**

Link: https://moodle.fct.unl.pt/course/view.php?id=6528

--//--

Nome: AED2021
Password: AEDMIEI2021

- **Práticas:**

Plataforma: Zoom  
Link (PO2): https://videoconf-colibri.zoom.us/j/84942305467?pwd=aGkyT3hLTDRRUzgwdWhqSzhGQlV1dz09  
(caso alguem tenha mais links para outras práticas online por favor mande-me)

--//--

Meeting ID: 849 4230 5467  
Password: dijkstra  

- **Discord:**

Discord para duvidas e discussão de código.

Link: https://discord.gg/PFZHmQ

- **Pasta Drive:** 

Link: https://drive.google.com/drive/folders/1ZT0z5d27NDoRo3mAY2jx42Z4fYr4bYZf?usp=sharing  
Aceder com a conta da Faculdade.

- **Exames/Testes:**

2020-11-13 / 19:00 / 02h00m / não requer inscrição prévia  
2021-01-16 / 09:00 / 02h00m / não requer inscrição prévia  

## Física

- **Aulas:**

Plataforma: Zoom  
Meeting ID: 949 2095 6704

- **Práticas:**

Plataforma: Zoom
Link: https://videoconf-colibri.zoom.us/j/96825991212?pwd=c2pwcWttYjhZNGx0ajM5ZlJhRE5QZz09  
Password: 516908

--//--

Meeting ID: 968 2599 1212  
Password: 516908

- **Bibliografia:** 

Inglês:  
Link: https://openstax.org/details/books/college-physics

- **Exames/Testes:**

2020-11-04 / 18:00 / 1h30m / Período de Inscricão: 2020-10-21 - 2020-10-28  
2021-01-05 / 18:30 / 1h30m / Período de Inscricão: 2020-12-21 - 2020-12-29  


## FSO | Fundamentos de Sistemas de Operação

- **Aulas:**

Plataforma: Zoom  
Turno T1 4ªf (12h00-13h30)  
Link: https://videoconf-colibri.zoom.us/j/97868501721  
Password: 259279  

Turno T1 5ªf (11h30-13h00)  
https://videoconf-colibri.zoom.us/j/91361518543  
Password: 259279  

--//--

Turno T1 4ªf (12h00-13h30)  
Meeting ID: 978 6850 1721  
Password: 259279  

Turno T1 5ªf (11h30-13h00)  
Meeting ID: 913 6151 8543  
Password: 259279  

- **Práticas:** 

Plataforma: Zoom
Link(PO1):https://videoconf-colibri.zoom.us/j/83735168079?pwd=cVJLeGFia3FhdDNkWnFhaU1EM0t5UT09  
(caso alguem tenha mais links para outras práticas online por favor mande-me)  

- **Pasta Drive:**

Link: https://drive.google.com/drive/folders/1VEaF9fnbB_6h6MKJR-LH2zM8ZsyHBjda?usp=sharing  
Aceder com a conta da Faculdade.  

- **Piazza:**

Link: https://piazza.com/class/kfd3uboxtnf2vn

- **Exames/Testes:**

2020-11-07 / 14:00 / 2h00m / não requer inscrição prévia  
2021-01-08 / 18:00 / 2h00m / não requer inscrição prévia

## LC | Lógica Computacional

- **Aulas:** 

Plataforma: Zoom   
Link: https://videoconf-colibri.zoom.us/j/96568474127

--//-- 

Meeting ID: 96568474127

- **Práticas:** 

Plataforma: Zoom
Link(PO3): https://videoconf-colibri.zoom.us/j/8579456730  
(caso alguem tenha mais links para outras práticas online por favor mande-me)

--//-- 

Meeting ID: 8579456730

- **Site da Cadeira:**

Link: http://lc.ssdi.di.fct.unl.pt/

- **Youtube:**

Link: https://www.youtube.com/channel/UC0QAdtrL7awJYzBxqKZu0dg

- **Zip to Tarsky's World:**

Link: https://drive.google.com/file/d/1YuMz06iJ6G63co8qE-D2VUbdW0qJoEca/view?usp=sharing

- **Exames/Testes:**

2020-10-19 / 19:30 / 1h00m / não requer inscrição prévia  
2020-11-10 / 19:00 / 1h00m / não requer inscrição prévia  
2020-11-30 / 19:30 / 1h00m / não requer inscrição prévia  
2021-01-12 / 09:00 / 1h00m / não requer inscrição prévia  



